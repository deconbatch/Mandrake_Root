* Mandrake Root.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2021/01/mandrake-root-shuffling-morphing-nodes.html]['Mandrake Root.' on deconbatch's Land of 1000 Creative Codings : Examples with Code.]]

[[./example01.png][An example image from this morphing animation.]]

** Shuffling the morphing nodes.
   - It is a creative coding morphing animation that is written in Processing. It creates an animation of a moving root or worm or something.
   - It turns several times and it shuffles the moving patterns every turn. It moves 'rootsFrom' to 'rootsTo' and the n-th root of 'rootsTo' is shuffled by 'j' in this code.
 int j = rootNo.get(i);
 drawRoot(rootsFrom.get(i), rootsTo.get(j)...);
   - This idea brought some attractions to this animation.
   - I made some other morphing animations. Please see this [[https://www.deconbatch.com/search/label/Morphing][link to my morphing animation works]].

** Change log.
   - created : 2021/01/20
